﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManMovement : MonoBehaviour
{
    [SerializeField]
    float MovementSpeed;

    public int TotalPoints;

    public int Lives = 3;

    AudioSource _audioSource;

    [SerializeField]
    AudioClip _audioMov;

    [SerializeField]
    AudioClip _deathSound;
    bool _hasPower;

    /// <summary>
    /// tempo trascorso da raccolta power up
    /// </summary>
    float _powerUpElapsedTime = 0;


    /// <summary>
    /// durata ultimo power up raccolto
    /// </summary>
    float _PowerUpDuration = 10;

    /// <summary>
    /// numero fantasmi durante power up
    /// </summary>
    int _eatenGhost;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        //Debug.Log("h: " + h + " - v: " + v);


        if (h != 0)
        {
            transform.Translate(Vector3.right * MovementSpeed * h);
        }
        else
        {
            transform.Translate(Vector3.forward * MovementSpeed * v);
        }

        if (h != 0 || v != 0)
        {
            if (!_audioSource.isPlaying)

                _audioSource.PlayOneShot(_audioMov);
        }

        if (_hasPower)
        {
            _powerUpElapsedTime += Time.deltaTime;
            Debug.Log("ELAPSED TIME " + _powerUpElapsedTime);

            if (_powerUpElapsedTime >= _PowerUpDuration)
            {
                _hasPower = false;
            }

        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("GNAM!");

        if (other.gameObject.tag == "pill")
        {
            OnEatPill(other);
        }

        if (other.gameObject.tag == "ghost")
        {
            if (!_hasPower)
                OnHit();
            else
            {
                _eatenGhost++;
                TotalPoints += (int)Mathf.Pow(2,_eatenGhost -1) * Ghost.Points;
                Destroy(other.gameObject);
            }

        }

    }

    void OnHit()
    {
        Debug.Log("Game Over!");

        if (!_audioSource.isPlaying)
        {
            _audioSource.Stop();
        }
        _audioSource.PlayOneShot(_deathSound);


        Lives -= 1;
    }
    void OnEatPill(Collider other)
    {
        Debug.Log("GNAM!");
        Pill pill = other.gameObject.GetComponent<Pill>();
        TotalPoints += pill.Points;

        if (pill is Powerup)
        {
            Debug.Log("Power Up");
            _hasPower = true;
            _powerUpElapsedTime = 0;

        }

        Destroy(other.gameObject);

    }
}
