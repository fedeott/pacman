﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILives : MonoBehaviour {
    [SerializeField]
    PacmanMovement pacman;
    [SerializeField]
    Text _text;

    private void Awake()
    {
       _text = GetComponent<Text>();
    }

    // Use this for initialization
    void Start ()
    {
       
	}
	
	// Update is called once per frame
	void Update ()
    {
        _text.text = pacman.Lives.ToString();
	}
}
