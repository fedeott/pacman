﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanMovement : MonoBehaviour
{
    [SerializeField]
    float MovementSpeed;
   
    public int TotalPoints;

    public int Lives = 3;

    AudioSource _audioSource;

    [SerializeField]
    AudioClip _audioMove;

    [SerializeField]
    AudioClip _audioDeath;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        //Debug.Log("h: " + h + " - v: " + v);
        if (h != 0)
        {
            transform.Translate(Vector3.right * h * MovementSpeed);
        }
        else
        {
            transform.Translate(Vector3.forward * v * MovementSpeed);
        }

        if (h != 0 || v != 0)
        {
            if(!_audioSource.isPlaying)
                _audioSource.PlayOneShot(_audioMove);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("GNAM!!");

        if (other.gameObject.tag == "pill")
        {
            Destroy(other.gameObject);
            Pill pill = other.gameObject.GetComponent<Pill>();
            TotalPoints += pill.Points;
        }

        if(other.tag == "ghost")
        {
            Debug.Log("SEI MORTO!!!");

            if (!_audioSource.isPlaying)
                _audioSource.PlayOneShot(_audioDeath);

            Lives -= 1;
        }

        
    }
}
