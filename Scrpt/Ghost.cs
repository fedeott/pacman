﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour {

    NavMeshAgent _navAgent;
    GameObject _player;

    [SerializeField]
    GameObject ghostMesh;

    private void Awake()
    {
        _navAgent = GetComponent<NavMeshAgent>();
    }

    // Use this for initialization
    void Start ()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _navAgent.SetDestination(_player.transform.position);
	}
	
	// Update is called once per frame
	void Update ()
    {
        ghostMesh.transform.position = transform.position;
        _navAgent.SetDestination(_player.transform.position);
    }
}
